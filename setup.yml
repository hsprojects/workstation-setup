# Manual steps:
#   - OS installation
#   - Give user sudo rights
#   - Install Git and Ansible
#   - Pull config repo
#   - ansible-galaxy install -r requirements.yml
#   - ansible-playbook setup.yml --ask-become-pass
#   - Open the Packer config file in nvim, source it and run 'PackerSync'
#   - Set Fish as terminal's default shell
#

- name: Configure machine
  hosts: localhost
  connection: local

  tasks:
    ############################################################ 
    # Set up makedeb and MPR
    ############################################################ 
    - name: Check if 'makedeb' is installed
      ansible.builtin.command: "which makedeb"
      register: which_makedeb
      failed_when: which_makedeb.rc > 1 # 'which' returns 1 when the binary is not found
      changed_when: false

    - name: Download makedeb installation script
      ansible.builtin.get_url:
        url: https://shlink.makedeb.org/install
        dest: /tmp/install-makedeb
        checksum: sha256:b34a2fbf08fff8ae56aa491876fa3a1715d4416784778107bc1d39a154102fdb
      when: which_makedeb.rc != 0


    - name: Run makedeb installation script
      ansible.builtin.command: "bash /tmp/install-makedeb"
      environment:
        MAKEDEB_RELEASE: makedeb
      when: which_makedeb.rc != 0

    - name: Download public key for MPR prebuilt repository
      become: yes
      ansible.builtin.get_url:
        url: https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub
        dest: /usr/share/keyrings/prebuilt-mpr-archive-keyring.asc
        checksum: sha256:14a92f5821f327f28b099fa405233011573a827e49fcb9a14d22005f3857c9c8

    - name: Add MPR prebuilt repo to APT repo list
      become: yes
      ansible.builtin.apt_repository:
        repo: 'deb [arch=all,amd64 signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.asc] https://proget.makedeb.org prebuilt-mpr bookworm'
        state: present

    ############################################################ 
    # Install dependencies
    ############################################################ 
    - name: Update cache and upgrade packages
      become: yes
      ansible.builtin.apt:
        update_cache: yes
        upgrade: yes

    - name: Install packages
      become: yes
      ansible.builtin.apt:
        pkg:
          - exa
          - fish
          - fzf
          - git
          - gnome-shell-extension-manager
          - gnome-tweaks
          - htop
          - just
          - neovim
          - nodejs
          - npm
          - python3-psutil
          - python3-venv
          - ripgrep
          - rsync
          - tmux
          - vlc

    ############################################################ 
    # "Install" user scripts
    ############################################################ 
    - name: '"Install" user scripts'
      ansible.builtin.file:
        src: '{{ playbook_dir }}/user_scripts'
        dest: ~/bin
        state: link

    ############################################################ 
    # Gnome settings
    ############################################################ 
    - name: Set keyboard layouts
      community.general.dconf:
        key: "/org/gnome/desktop/input-sources/sources"
        value: "[('xkb', 'us+intl'), ('xkb', 'at'), ('xkb', 'gr')]"
        state: present

    - name: Register custom keybindngs map for Gnome keyboard
      community.general.dconf:
        key: "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings"
        value: "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
        state: present

    - name: Set terminal shortcut key binding
      community.general.dconf:
        key: "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/binding"
        value: "'<Super>Return'"
        state: present

    - name: Set terminal shortcut command
      community.general.dconf:
        key: "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/command"
        value: "'x-terminal-emulator'"
        state: present

    - name: Set terminal shortcut name
      community.general.dconf:
        key: "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/name"
        value: "'Terminal'"
        state: present

    - name: Swap Ctrl and Caps Lock
      community.general.dconf:
        key: "/org/gnome/desktop/input-sources/xkb-options"
        value: "['ctrl:swapcaps']"
        state: present

    - name: Enable touchpad tap-to-click
      community.general.dconf:
        key: "/org/gnome/desktop/peripherals/touchpad/tap-to-click"
        value: "true"
        state: present

    ############################################################ 
    # Git settings
    ############################################################ 
    - name: Configure the 'git lg' alias
      community.general.git_config:
        name: alias.lg
        scope: global
        value: 'log --oneline --decorate --graph'

    - name: Configure the 'git co' alias
      community.general.git_config:
        name: alias.co
        scope: global
        value: checkout

    - name: Configure the 'git amend' alias
      community.general.git_config:
        name: alias.amend
        scope: global
        value: 'commit --amend'

    - name: Configure git user name
      community.general.git_config:
        name: user.name
        scope: global
        value: 'Pieter-Jan Smets'

    - name: Set Neovim as Git editor
      community.general.git_config:
        name: core.editor
        scope: global
        value: nvim

    ############################################################ 
    # Neovim settings
    ############################################################ 
    - name: Install Packer to manage Neovim dependencies
      ansible.builtin.git:
        repo: 'https://github.com/wbthomason/packer.nvim'
        dest: '~/.local/share/nvim/site/pack/packer/start/packer.nvim'

    - name: Copy Neovim config to correct location
      ansible.posix.synchronize:
        src: config_files/nvim
        dest: '~/.config/'

    ############################################################ 
    # Tmux settings
    ############################################################ 
    - name: Set tmux settings
      ansible.builtin.file:
        src: '{{ playbook_dir }}/config_files/tmux.conf'
        dest: ~/.tmux.conf
        state: link

    ############################################################ 
    # Fish settings
    ############################################################ 
    - name: Create Fish config directory
      ansible.builtin.file:
        path: ~/.config/fish/conf.d
        state: directory

    - name: Set Fish config file
      ansible.builtin.file:
        src: '{{ playbook_dir }}/config_files/fish/config.fish'
        dest: ~/.config/fish/conf.d/config.fish
        state: link

    - name: Set Fish as user's shell
      become: true
      ansible.builtin.user:
        name: pyjamas
        shell: /usr/bin/fish

    ############################################################ 
    # Install pyenv
    ############################################################ 
    - name: Install Pyenv
      ansible.builtin.git:
        repo: 'https://github.com/pyenv/pyenv.git'
        dest: '~/.pyenv'

    - name: Check if 'pyenv' is on the Fish PATH
      ansible.builtin.command: "/usr/bin/fish -c 'which pyenv'"
      register: fish_path
      changed_when: false

    - name: Add Pyenv to the Fish path
      ansible.builtin.command: "/usr/bin/fish -c 'fish_add_path $HOME/.pyenv/bin'"
      when: fish_path.stdout.find('pyenv') == 0

