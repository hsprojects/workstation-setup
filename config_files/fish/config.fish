# Shell navigation
abbr --add ls 'exa'
abbr --add ll 'exa -la'
abbr --add tree 'exa --tree -l'
abbr --add treea 'exa --tree -la'
abbr --add code 'code .'
abbr --add c 'code .'

# Gcloud & Kubernetes
abbr --add ecs 'env-connect staging'
abbr --add ecsr 'env-connect -r staging'
abbr --add ecd 'env-connect demo'
abbr --add ecdr 'env-connect -r demo'
abbr --add ecppd 'env-connect preprod'
abbr --add ecppdr 'env-connect -r preprod'

abbr --add k9s 'k9s --kubeconfig $KUBECONFIG'

# Git
abbr --add gaa 'git add -A'
abbr --add gp 'git pull'
abbr --add gpr 'git pull --rebase'
abbr --add gf 'git fetch'

# Logs
abbr --add prdlogs 'xdg-open https://cloudlogging.app.goo.gl/kvc2mqiLdA9b4rFv6'
abbr --add ppdlogs 'xdg-open https://cloudlogging.app.goo.gl/4xpSi7VPkwq1hJfn6'
abbr --add demlogs 'xdg-open https://cloudlogging.app.goo.gl/GDMT38pRgTdHQdsN9'
abbr --add stglogs 'xdg-open https://cloudlogging.app.goo.gl/oekZt2N9PDtdRbcv9'

# System tools
abbr --add start 'sudo systemctl start'
abbr --add stop 'sudo systemctl stop'
abbr --add restart 'sudo systemctl restart'
abbr --add update 'sudo apt-get update && sudo apt-get upgrade'
abbr --add install 'sudo apt-get install'

# Docker
abbr --add dc 'docker-compose'
abbr --add dmanage "docker ps | grep debug_web | cut -f 1 -d ' ' | xargs -I '{}' docker exec {} ./manage.py"
abbr --add dockertest "docker ps | grep debug_web | cut -f 1 -d ' ' | xargs -I '{}' docker exec {} ./manage.py test"

# Vim
abbr --add vim 'nvim'
abbr --add v 'nvim .'

# Other
abbr --add kb 'kustomize build'
abbr --add makephonies "grep -Po '^[a-zA-Z0-9-_]*' Makefile | xargs echo '.PHONY:' >> Makefile"
abbr --add tmuxd 'tmux attach-session -t default; or tmux new-session -t default'


# Enable pyenv to shim python
pyenv init - | source

