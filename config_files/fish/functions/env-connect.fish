#!/bin/env fish


############################################################
# Helper functions
############################################################
function info --argument message
	echo "[INFO] $message"
end

function set_kubeconfig --argument environment
	set -g -x KUBECONFIG ~/.kube/projects/$environment
	info "KUBECONFIG changed to $KUBECONFIG"
end

function create_kubectl_config --argument environment
  # Use new auth plugin https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke
  set -x USE_GKE_GCLOUD_AUTH_PLUGIN True
	switch $environment
		case staging
			info "Recreating staging auth config"
			gcloud container clusters get-credentials yel-prolive-stg-kub-sirius \
				--region europe-west1 \
				--project yel-sirius-stg-prj-001
		case demo
			info "Recreating demo auth config"
			gcloud container clusters get-credentials yel-prolive-dem-kub-sirius \
				--region europe-west1 \
				--project yel-sirius-dem-prj-001
		case preprod
			info "Recreating preprod auth config"
			gcloud container clusters get-credentials yel-prolive-ppd-kub-sirius \
				--region europe-west1 \
				--project yel-sirius-ppd-prj-001
		case production
			info "Recreating production auth config"
			gcloud container clusters get-credentials yel-prolive-prd-kub-sirius \
				--region europe-west1 \
				--project yel-sirius-prd-prj-001
		case bridge-dev
			info "Recreating production auth config"
			gcloud container clusters get-credentials ldf-bridge-dev-gke-001 \
				--region europe-west1 \
				--project ldf-bridge-dev-prj-001
		case bridge-production
			info "Recreating production auth config"
			gcloud container clusters get-credentials ldf-bridge-prd-gke-001 \
				--region europe-west1 \
				--project ldf-bridge-prd-prj-001
	end
end

function set_gcloud_project --argument environment
	switch $environment
		case staging
			info "Setting staging gcloud project"
			gcloud config set project yel-sirius-stg-prj-001
		case demo
			info "Setting demo gcloud project"
			gcloud config set project yel-sirius-dem-prj-001
		case preprod
			info "Setting preprod gcloud project"
			gcloud config set project yel-sirius-ppd-prj-001
		case production
			info "Setting production gcloud project"
			gcloud config set project yel-sirius-prd-prj-001
		case bridge-dev
			info "Setting production gcloud project"
			gcloud config set project ldf-bridge-dev-prj-001
		case bridge-production
			info "Setting production gcloud project"
			gcloud config set project ldf-bridge-prd-prj-001
	end
end


############################################################
# Main function
############################################################
function env-connect
	argparse -N 1 --name=connect 'r/reconnect' -- $argv 

	set environment $argv[1]

	# filter out invalid environment names
	switch $environment
		case staging
		case demo
		case preprod
		case production
		case stg
			set environment "staging"
		case ppd
			set environment "preprod"	
		case bridge-dev
		case bridge-production
		case '*'
			echo "[ERROR] Unknown environment: $environment"
			exit 1
	end

	# set the path to the config file for kubectl	
	set_kubeconfig $environment

	# recreate kubectl config if requested
	if set -q _flag_r
		create_kubectl_config $environment
	end

	# configure default gcloud project
	set_gcloud_project $environment
end

