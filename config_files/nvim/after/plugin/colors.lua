function SetColorScheme(scheme)
	scheme = scheme or "melange"
	vim.cmd.colorscheme(scheme)
end

vim.opt.termguicolors = true
SetColorScheme()
